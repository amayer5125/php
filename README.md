# Docker PHP

A Docker image used for testing and deploying PHP applications

## Building

```sh
docker build -t registry.gitlab.com/amayer5125/php:7.4 .
```
